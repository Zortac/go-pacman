package main

import (
	"github.com/nsf/termbox-go"
)

const (
	EVT_IO_UP int = iota
	EVT_IO_DOWN
	EVT_IO_LEFT
	EVT_IO_RIGHT
	EVT_IO_RESIZE
	EVT_SIG_EXIT
)

var width, height int

func main() {
	ioQueue := make(chan int)
	sigQueue := make(chan int)

	if err := termbox.Init(); err != nil {
		panic(err)
	}
	defer termbox.Close()

	width, height := termbox.Size()

	go ioSource(ioQueue, sigQueue)

	counter := uint8(0)

	for {
		select {
		case ioMsg := <-ioQueue:
			switch ioMsg {
			case EVT_IO_UP:
				termbox.SetCell(width/2, height/2, '↑', termbox.ColorRed, termbox.ColorBlack)
			case EVT_IO_DOWN:
				termbox.SetCell(width/2, height/2, '↓', termbox.ColorRed, termbox.ColorBlack)
			case EVT_IO_LEFT:
				termbox.SetCell(width/2, height/2, '←', termbox.ColorRed, termbox.ColorBlack)
			case EVT_IO_RIGHT:
				termbox.SetCell(width/2, height/2, '→', termbox.ColorRed, termbox.ColorBlack)
			}
		case sigMsg := <-sigQueue:
			if sigMsg == EVT_SIG_EXIT {
				return
			}
		default:
			// Do nothing
		}

		termbox.SetCell(width/2-1, height/2, int32(counter), termbox.ColorRed, termbox.ColorBlack)
		counter++

		// Display any changes made in the game loop
		if err := termbox.Flush(); err != nil {
			panic(err)
		}
	}

}

func ioSource(ioq chan int, sigq chan int) {
	for {
		evt := termbox.PollEvent()
		if evt.Type == termbox.EventKey && evt.Ch != 0 {
			switch evt.Ch {
			case 'w':
				ioq <- EVT_IO_UP
			case 's':
				ioq <- EVT_IO_DOWN
			case 'a':
				ioq <- EVT_IO_LEFT
			case 'd':
				ioq <- EVT_IO_RIGHT
			}
		} else if evt.Type == termbox.EventResize {
			width, height = evt.Width, evt.Height
			ioq <- EVT_IO_RESIZE
		} else if evt.Type == termbox.EventKey && evt.Key == termbox.KeyEsc {
			sigq <- EVT_SIG_EXIT
		}
	}
}
