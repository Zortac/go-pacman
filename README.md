# go-pacman
`go-pacman` is a simple Go implementation of the iconic PacMan game for a Go
course I'm teaching. It uses the great [termbox](https://github.com/nsf/termbox-go) library for terminal graphics.

## Build and run
If you want to build the Application (I don't know why you would want to do
that, as it's under heavy development and not functional in any way), here's how
to do that:

Clone the repository and change directory:
```
git clone https://gitlab.com/zortac/go-pacman.git
cd go-pacman
```

Get dependencies and build:
```
go get
go build
```

Run (UNIX-like systems):
```
./go-pacman
```

Run (Windows):
```
go-pacman.exe
```

# License
This code is placed into the public domain. For details see
[LICENSE](https://gitlab.com/Zortac/go-pacman/blob/master/LICENSE).
`termbox` is licensed unter the MIT License.
